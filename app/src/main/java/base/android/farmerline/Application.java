package base.android.farmerline;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.multidex.MultiDexApplication;

import base.android.farmerline.features.agentoverview.AgentOverviewActivity;
import base.android.farmerline.features.login.LoginActivity;

public class Application extends MultiDexApplication {

	static SharedPreferences prefs;

	@Override
	public void onCreate() {
		super.onCreate();
		initializePrefs();
	}

	public void checkIfUserIsSignedIn(){
		if ("".equals(Application.getUserName())){
			goToLogin();
		}else{
			goToHome();
		}
	}

	public void goToHome(){
		Intent mapsIntent = new Intent(this, AgentOverviewActivity.class);
		startActivity(mapsIntent);
	}

	public void goToLogin(){
		Intent loginActivity = new Intent(this, LoginActivity.class);
		startActivity(loginActivity);
	}

	public void initializePrefs() {
		prefs = getSharedPreferences("com.base.android.farmerline.file", Context.MODE_PRIVATE);
	}

	public static void setUserName(String userName) {
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString("userName", userName);
		editor.apply();
	}

	public static String getUserName() {
		return prefs.getString("userName", "");
	}

	public static void setEmail(String email) {
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString("email", email);
		editor.apply();
	}

	public static String getEmail() {
		return prefs.getString("email", "");
	}

	public static void setImage(String image) {
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString("image", image);
		editor.apply();
	}

	public static String getImage() {
		return prefs.getString("image", "");
	}

	public static void setLatitude(double latitude) {
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString("latitude", String.valueOf(latitude));
		editor.apply();
	}

	public static double getLatitude() {
		String latitude = prefs.getString("latitude", "0");
		return Double.valueOf(latitude);
	}

	public static void setLongitude(double longitude) {
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString("longitude", String.valueOf(longitude));
		editor.apply();
	}

	public static double getLongitude() {
		String latitude = prefs.getString("longitude", "");
		return Double.valueOf(latitude);
	}





}
