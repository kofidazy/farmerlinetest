package base.android.farmerline.features.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import java.util.Arrays;

import base.android.farmerline.features.agentoverview.AgentOverviewActivity;
import base.android.farmerline.Application;
import base.android.farmerline.R;


public class LoginActivity extends AppCompatActivity implements MainViewContract {

	EditText name;
	EditText password;
	Button login_button;
	GoogleSignInClient googleSignInClient;
	SignInButton signInButton;
	CallbackManager callbackManager;
	LoginButton loginButton;
	private static final String EMAIL = "email";

	public static int RC_SIGN_IN = 12;
	public static String TAG = "LoginActivity";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);


		GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
				.requestEmail()
				.build();

		googleSignInClient = GoogleSignIn.getClient(this, gso);
		signInButton = findViewById(R.id.sign_in_button);

		attachListeners();

		checkIfUserIsSignedIn();

		callbackManager = CallbackManager.Factory.create();

		LoginManager.getInstance().registerCallback(callbackManager,
				new FacebookCallback<LoginResult>() {
					@Override
					public void onSuccess(LoginResult loginResult) {
						goToHome();
					}

					@Override
					public void onCancel() {
					}

					@Override
					public void onError(FacebookException exception) {

					}
				});



	}


	@Override
	public String showSum(int sum) {
		return String.valueOf(sum);
	}

	public void updateUI(GoogleSignInAccount googleSignInAccount){

	}

	public void attachListeners(){
		signInButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
					case R.id.sign_in_button:
						signIn();
						break;
				}
			}
		});

		loginButton = (LoginButton) findViewById(R.id.login_button);
		loginButton.setReadPermissions(Arrays.asList(EMAIL));

	}

	private void signIn() {
		Intent signInIntent = googleSignInClient.getSignInIntent();
		startActivityForResult(signInIntent, RC_SIGN_IN);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		callbackManager.onActivityResult(requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == RC_SIGN_IN) {
			Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
			handleSignInResult(task);
		}
	}

	private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
		try {
			GoogleSignInAccount account = completedTask.getResult(ApiException.class);
			Application.setUserName(account.getDisplayName());
			Application.setEmail(account.getEmail());
			if (account.getPhotoUrl() != null) {
				Application.setImage(account.getPhotoUrl().toString());
			}
			goToHome();
			finish();

		} catch (ApiException e) {
			Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
			updateUI(null);
		}
	}

	public void checkIfUserIsSignedIn(){
		if ("".equals(Application.getUserName())){

		}else{
			goToHome();
		}
	}

	public void goToHome(){
		Intent mapsIntent = new Intent(this, AgentOverviewActivity.class);
		startActivity(mapsIntent);
	}
}
