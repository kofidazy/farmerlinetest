package base.android.farmerline.features.map;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import base.android.farmerline.Application;
import base.android.farmerline.PermissionUtils;
import base.android.farmerline.R;
import base.android.farmerline.features.DialogShowLoader;
import base.android.farmerline.features.login.LoginActivity;

public class MapsActivity extends AppCompatActivity implements
		OnMyLocationButtonClickListener,
		OnMyLocationClickListener,
		OnMapReadyCallback,
		GoogleMap.OnMarkerClickListener,
		GoogleApiClient.OnConnectionFailedListener,
		ActivityCompat.OnRequestPermissionsResultCallback {

	private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
	private String TAG = "MapsActivity";
	private boolean mPermissionDenied = false;
	private FusedLocationProviderClient mFusedLocationClient;
	private static final LatLngBounds LAT_LNG_BOUNDS = new LatLngBounds(
			new LatLng(-40, -168), new LatLng(71, 136));

	private PlaceAutocompleteAdapter mPlaceAutocompleteAdapter;
	private GoogleApiClient mGoogleApiClient;
	private static final float DEFAULT_ZOOM = 15f;

	private GoogleMap mMap;
	private DrawerLayout drawerLayout;
	private AutoCompleteTextView searchView;
	private ImageView avatar;
	private TextView name;
	private TextView email;
	NavigationView navigationView;
	DialogShowLoader materialDialog;
	RelativeLayout locateme;
	double num = 0.00;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_maps);

		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setTitle("Agent Overview");
		toolbar.setTitleTextColor(Color.WHITE);
		toolbar.setSubtitleTextColor(Color.WHITE);


		ActionBar actionbar = getSupportActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);

		drawerLayout = findViewById(R.id.drawer_layout);
		searchView = findViewById(R.id.search_value);
		locateme = findViewById(R.id.locateme);

		navigationView = (NavigationView) findViewById(R.id.nav_view);
		View headerView = navigationView.getHeaderView(0);
		name = headerView.findViewById(R.id.name);
		email = headerView.findViewById(R.id.email);
		avatar = headerView.findViewById(R.id.avatar);
		final Context context = this;


		askLocationPermissions();
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);

		mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

		configureUser();

		navigationView.setNavigationItemSelectedListener(
				new NavigationView.OnNavigationItemSelectedListener() {
					@Override
					public boolean onNavigationItemSelected(MenuItem menuItem) {
						menuItem.setChecked(true);
						logout();
						return true;
					}
				});

		locateme.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				locateMe(context);
			}
		});
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				drawerLayout.openDrawer(GravityCompat.START);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void geoLocate() {
		Log.d(TAG, "geoLocate: geolocating");

		String searchString = searchView.getText().toString();

		Geocoder geocoder = new Geocoder(this);
		List<Address> list = new ArrayList<>();
		try {
			list = geocoder.getFromLocationName(searchString, 1);
		} catch (IOException e) {
			Log.e(TAG, "geoLocate: IOException: " + e.getMessage());
		}

		if (list.size() > 0) {
			Address address = list.get(0);

			Log.d(TAG, "geoLocate: found a location: " + address.toString());

			moveCamera(new LatLng(address.getLatitude(), address.getLongitude()), DEFAULT_ZOOM,
					address.getAddressLine(0));
		}
	}

	private void moveCamera(LatLng latLng, float zoom, String title) {
		Log.d(TAG, "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " + latLng.longitude);
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

		if (!title.equals("My Location")) {
			MarkerOptions options = new MarkerOptions()
					.position(latLng)
					.title(title);
			mMap.addMarker(options);
		}
	}


	@Override
	public void onMapReady(GoogleMap map) {
		mMap = map;

		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
				&& ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			return;
		}

		mGoogleApiClient = new GoogleApiClient
				.Builder(this)
				.addApi(Places.GEO_DATA_API)
				.addApi(Places.PLACE_DETECTION_API)
				.enableAutoManage(this, this)
				.build();

		mGoogleApiClient.connect();

		mPlaceAutocompleteAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient,
				LAT_LNG_BOUNDS, null);

		searchView.setAdapter(mPlaceAutocompleteAdapter);

		searchView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH
						|| actionId == EditorInfo.IME_ACTION_DONE
						|| keyEvent.getAction() == KeyEvent.ACTION_DOWN
						|| keyEvent.getAction() == KeyEvent.KEYCODE_ENTER) {

					geoLocate();
				}

				return false;
			}
		});


		mMap.setMyLocationEnabled(false);
		mMap.getUiSettings().setMyLocationButtonEnabled(false);
		mMap.setOnMyLocationButtonClickListener(this);
		mMap.setOnMyLocationClickListener(this);

		if (num != Application.getLatitude()) {
			LatLng junctionMall = new LatLng(Application.getLatitude(), Application.getLongitude());
			MarkerOptions marker = new MarkerOptions().position(junctionMall).title("My location");
			marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.new_marker));
			mMap.addMarker(marker);
			mMap.moveCamera(CameraUpdateFactory.newLatLng(junctionMall));
			mMap.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);
		}else {

			mFusedLocationClient.getLastLocation()
					.addOnSuccessListener(this, new OnSuccessListener<Location>() {
						@Override
						public void onSuccess(Location location) {
							if (location != null) {
								Application.setLatitude(location.getLatitude());
								Application.setLongitude(location.getLongitude());
								LatLng junctionMall = new LatLng(location.getLatitude(), location.getLongitude());
								MarkerOptions marker = new MarkerOptions().position(junctionMall).title("My location");
								marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.new_marker));
								mMap.addMarker(marker);
								mMap.moveCamera(CameraUpdateFactory.newLatLng(junctionMall));
								mMap.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null);
							}
						}
					});
		}

		final Context context = this;

		mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
			@Override
			public boolean onMarkerClick(final Marker marker) {
				showLoading("Saving Location...", context);
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						Application.setLatitude(marker.getPosition().latitude);
						Application.setLongitude(marker.getPosition().longitude);
						hideLoading();
						finish();
					}
				}, 3000);

				return true;
			}
		});

	}


	private void enableMyLocation() {
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
				!= PackageManager.PERMISSION_GRANTED) {
			PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
					Manifest.permission.ACCESS_FINE_LOCATION, true);
		} else if (mMap != null) {
			mMap.setMyLocationEnabled(true);
		}
	}

	@Override
	public boolean onMyLocationButtonClick() {
		return false;
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
	                                       @NonNull int[] grantResults) {
		if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
			return;
		}

		if (PermissionUtils.isPermissionGranted(permissions, grantResults,
				Manifest.permission.ACCESS_FINE_LOCATION)) {
			enableMyLocation();
		} else {
			mPermissionDenied = true;
		}
	}

	public void askLocationPermissions() {
		try {
			if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
				ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 101);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onMyLocationClick(@NonNull Location location) {
	}

	@Override
	public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

	}

	public void configureUser() {
		RequestOptions options = new RequestOptions();
		options.circleCrop();
		if (!"".equals(Application.getImage())) {
			Glide.with(this)
					.load(Uri.parse(Application.getImage()))
					.apply(options)
					.into(avatar);
		}
		name.setText(Application.getUserName());
		email.setText(Application.getEmail());
	}

	public void logout() {
		Application.setImage("");
		Application.setEmail("");
		Application.setUserName("");
		Application.setLongitude(0);
		Application.setLatitude(0);
		goToLogin();

	}

	public void goToLogin() {
		Intent loginActivity = new Intent(this, LoginActivity.class);
		startActivity(loginActivity);
		finish();
	}

	public boolean onMarkerClick(final Marker marker) {

		// Retrieve the data from the marker.
		Integer clickCount = (Integer) marker.getTag();

		// Check if a click count was set, then display the click count.
		if (clickCount != null) {
			clickCount = clickCount + 1;
			marker.setTag(clickCount);
			Toast.makeText(this,
					marker.getTitle() +
							" has been clicked " + clickCount + " times.",
					Toast.LENGTH_SHORT).show();
		}

		return false;
	}

	public void showLoading(String message, Context context) {
		FragmentManager fm = getSupportFragmentManager();
		materialDialog = DialogShowLoader.newInstance("", message);
		materialDialog.show(fm, "loader");
	}

	public void hideLoading() {
		if (materialDialog != null) {
			materialDialog.dismiss();
		}
	}

	public void locateMe(final Context context) {
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			return;
		}
		mMap.setMyLocationEnabled(false);
		mMap.getUiSettings().setMyLocationButtonEnabled(false);
		mMap.setOnMyLocationButtonClickListener(this);
		mMap.setOnMyLocationClickListener(this);

		mFusedLocationClient.getLastLocation()
				.addOnSuccessListener(this, new OnSuccessListener<Location>() {
					@Override
					public void onSuccess(Location location) {
						if (location != null) {
							Application.setLatitude(location.getLatitude());
							Application.setLongitude(location.getLongitude());
							LatLng junctionMall = new LatLng(location.getLatitude(), location.getLongitude());
							MarkerOptions marker = new MarkerOptions().position(junctionMall).title("My location");
							marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.new_marker));
							mMap.addMarker(marker);
							mMap.moveCamera(CameraUpdateFactory.newLatLng(junctionMall));
							mMap.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null);

							showLoading("Saving Location...", context);

							new Handler().postDelayed(new Runnable() {
								@Override
								public void run() {
									hideLoading();
									finish();
								}
							}, 2600);


						}
					}
				});
	}
}
