package base.android.farmerline.features.login;

public interface MainPresenterContract {

	public int sum(int a, int b);
	public boolean validatePassword(String password);
	public boolean validatePhoneNumber(String phoneNumber);
}
