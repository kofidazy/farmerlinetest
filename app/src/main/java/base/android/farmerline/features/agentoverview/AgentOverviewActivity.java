package base.android.farmerline.features.agentoverview;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.ValueDependentColor;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import base.android.farmerline.Application;
import base.android.farmerline.R;
import base.android.farmerline.features.login.LoginActivity;
import base.android.farmerline.features.map.MapsActivity;

public class AgentOverviewActivity extends AppCompatActivity implements
		GoogleMap.OnMyLocationButtonClickListener,
		GoogleMap.OnMyLocationClickListener,
		OnMapReadyCallback,
		ActivityCompat.OnRequestPermissionsResultCallback {

	DrawerLayout drawerLayout;
	GraphView graph;
	RelativeLayout add_location_button;
	RelativeLayout current_location;
	RelativeLayout map_cover;
	private GoogleMap mMap;
	private ImageView avatar;
	private TextView name;
	private TextView email;
	NavigationView navigationView;
	double num = 0.00;



	protected static final int REQUEST_CHECK_SETTINGS = 0x1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_agent_overview);

		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setTitle("Agent Overview");
		toolbar.setTitleTextColor(Color.WHITE);
		toolbar.setSubtitleTextColor(Color.WHITE);


		ActionBar actionbar = getSupportActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);

		drawerLayout = findViewById(R.id.drawer_layout);
		add_location_button = findViewById(R.id.add_location_button);
		current_location = findViewById(R.id.current_location);
		map_cover = findViewById(R.id.map_cover);

		navigationView= (NavigationView) findViewById(R.id.nav_view);
		View headerView = navigationView.getHeaderView(0);
		name = headerView.findViewById(R.id.name);
		email = headerView.findViewById(R.id.email);
		avatar = headerView.findViewById(R.id.avatar);

		askLocationPermissions();
		displayLocationSettingsRequest(this);

		attachListeners();
		configureCharts();
		configureUser();
		if (num != Application.getLatitude()) {
			add_location_button.setVisibility(View.GONE);
			current_location.setVisibility(View.VISIBLE);
			configureMap();
		} else {
			add_location_button.setVisibility(View.VISIBLE);
			current_location.setVisibility(View.GONE);
		}


	}

	@Override
	protected void onResume() {
		super.onResume();
		if (num != Application.getLatitude()) {
			add_location_button.setVisibility(View.GONE);
			current_location.setVisibility(View.VISIBLE);
			configureMap();
		} else {
			add_location_button.setVisibility(View.VISIBLE);
			current_location.setVisibility(View.GONE);
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				drawerLayout.openDrawer(GravityCompat.START);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void attachListeners() {
		final Context context = this;
		add_location_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent mapsIntent = new Intent(context, MapsActivity.class);
				context.startActivity(mapsIntent);
			}
		});

		map_cover.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent mapsIntent = new Intent(context, MapsActivity.class);
				context.startActivity(mapsIntent);
			}
		});

		navigationView.setNavigationItemSelectedListener(
				new NavigationView.OnNavigationItemSelectedListener() {
					@Override
					public boolean onNavigationItemSelected(MenuItem menuItem) {
						// set item as selected to persist highlight
						menuItem.setChecked(true);
						// close drawer when item is tapped
						logout();

						// Add code here to update the UI based on the item selected
						// For example, swap UI fragments here

						return true;
					}
				});
	}

	public void askLocationPermissions() {
		try {
			if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
				ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 101);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void displayLocationSettingsRequest(Context context) {
		final Activity context1 = this;
		GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
				.addApi(LocationServices.API).build();
		googleApiClient.connect();

		LocationRequest locationRequest = LocationRequest.create();
		locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		locationRequest.setInterval(10000);
		locationRequest.setFastestInterval(10000 / 2);

		LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
		builder.setAlwaysShow(true);

		PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
		result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
			@Override
			public void onResult(LocationSettingsResult result) {
				final Status status = result.getStatus();
				switch (status.getStatusCode()) {
					case LocationSettingsStatusCodes.SUCCESS:
						Log.i("MapsActivity", "All location settings are satisfied.");
						break;
					case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
						Log.i("MapsActivity", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

						try {
							status.startResolutionForResult(context1, REQUEST_CHECK_SETTINGS);
						} catch (IntentSender.SendIntentException e) {
							Log.i("MapsActivity", "PendingIntent unable to execute request.");
						}
						break;
					case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
						Log.i("MapsActivity", "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
						break;
				}
			}
		});
	}

	public void configureCharts() {
		graph = (GraphView) findViewById(R.id.chart_view);
		BarGraphSeries<DataPoint> series = new BarGraphSeries<>(new DataPoint[]{
				new DataPoint(0, 0),
				new DataPoint(2, 28000),
				new DataPoint(5, 44000),
				new DataPoint(8, 13000),
		});
		graph.addSeries(series);

		// styling
		series.setValueDependentColor(new ValueDependentColor<DataPoint>() {
			@Override
			public int get(DataPoint data) {
				return Color.rgb(1, 130, 255);
			}
		});

//		series.setSpacing(20);

		series.setDrawValuesOnTop(true);
		series.isAnimated();
		series.setValuesOnTopColor(Color.BLACK);
		//series.setValuesOnTopSize(50);
	}

	public void configureMap() {
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);
	}

	public void configureUser() {
		RequestOptions options = new RequestOptions();
		options.circleCrop();
		if (!"".equals(Application.getImage())) {
			Glide.with(this)
					.load(Uri.parse(Application.getImage()))
					.apply(options)
					.into(avatar);
		}
		name.setText(Application.getUserName());
		email.setText(Application.getEmail());
	}

	@Override
	public void onMapReady(GoogleMap map) {
		mMap = map;

		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
				&& ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			return;
		}

		mMap.setMyLocationEnabled(false);
		mMap.getUiSettings().setMyLocationButtonEnabled(false);
		mMap.setOnMyLocationButtonClickListener(this);
		mMap.setOnMyLocationClickListener(this);



		if (num != Application.getLatitude()) {
			LatLng junctionMall = new LatLng(Application.getLatitude(), Application.getLongitude());
			MarkerOptions marker = new MarkerOptions().position(junctionMall).title("My Location");
			marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.small_marker));
			mMap.addMarker(marker);
			mMap.moveCamera(CameraUpdateFactory.newLatLng(junctionMall));
			mMap.animateCamera(CameraUpdateFactory.zoomTo(13), 2000, null);
		}

	}

	@Override
	public boolean onMyLocationButtonClick() {
		return false;
	}

	@Override
	public void onMyLocationClick(@NonNull Location location) {

	}

	public void logout(){
		Application.setImage("");
		Application.setEmail("");
		Application.setUserName("");
		Application.setLongitude(0);
		Application.setLatitude(0);
		goToLogin();

	}

	public void goToLogin(){
		Intent loginActivity = new Intent(this, LoginActivity.class);
		startActivity(loginActivity);
		finish();
	}
}
