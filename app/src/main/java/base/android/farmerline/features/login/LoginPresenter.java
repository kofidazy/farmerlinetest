package base.android.farmerline.features.login;

public class LoginPresenter implements MainPresenterContract {

	MainViewContract mainViewContract;

	@Override
	public int sum(int a, int b) {
		return  a + b;
	}

	@Override
	public boolean validatePassword(String password) {
		if (password.length() < 6){
			return false;
		}
		return true;
	}

	@Override
	public boolean validatePhoneNumber(String phoneNumber) {
		if (phoneNumber.length() < 6){
			return false;
		}
		return true;
	}

	LoginPresenter(MainViewContract mainViewContract){
		this.mainViewContract = mainViewContract;
	}



}
